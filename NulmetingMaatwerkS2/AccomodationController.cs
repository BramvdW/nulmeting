﻿using System;
using System.Collections.Generic;
using System.Text;
using NulmetingMaatwerkS2.HomeTypes;

namespace NulmetingMaatwerkS2
{
    public class AccomodationController
    {
        public bool AddNewBungalow(Bungalow bungalow)
        {
            if (String.IsNullOrWhiteSpace(bungalow.amountOfPeaple.ToString()) || String.IsNullOrWhiteSpace(bungalow.amountOfsquareMeters.ToString()) || String.IsNullOrWhiteSpace(bungalow.nextToWater.ToString()) || String.IsNullOrWhiteSpace(bungalow.price.ToString()) || String.IsNullOrWhiteSpace(bungalow.variant))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool AddNewHotelRoom(HotelRoom hotelRoom)
        {
            if (String.IsNullOrWhiteSpace(hotelRoom.amountOfPeaple.ToString()) || String.IsNullOrWhiteSpace(hotelRoom.amountOfsquareMeters.ToString()) || String.IsNullOrWhiteSpace(hotelRoom.bedType.ToString()) || String.IsNullOrWhiteSpace(hotelRoom.price.ToString()) || String.IsNullOrWhiteSpace(hotelRoom.facilitys))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool AddNewHotelRoom(Tent tent)
        {
            if (String.IsNullOrWhiteSpace(tent.amountOfPeaple.ToString()) || String.IsNullOrWhiteSpace(tent.amountOfsquareMeters.ToString())  || String.IsNullOrWhiteSpace(tent.price.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
