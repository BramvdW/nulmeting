﻿using System;
using System.Collections.Generic;
using System.Text;
using NulmetingMaatwerkS2.HomeTypes;

namespace NulmetingMaatwerkS2
{
    public enum HomeType
    {
        Bungalow,
        HotelRoom,
        Tent
    }

    public class Accommodation
    {
        public HomeType homeType { get; set; }
        public int amountOfPeaple { get; set; }
        
        public Bungalow bungalow { get; set; }
        public HotelRoom hotelRoom { get; set; }
        public Tent tent { get; set; }
        public Defect defect { get; set; }

       public Accommodation(HomeType homeType, int amountOfPeaple)
        {
            this.homeType = homeType;
            this.amountOfPeaple = amountOfPeaple;
        }

       

    }
}
