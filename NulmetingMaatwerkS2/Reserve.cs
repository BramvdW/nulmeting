﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NulmetingMaatwerkS2
{
    public enum OptionalCases
    {
        Nothing,
        ChildBed,
        Bedding
    }
    public class Reserve
    {
        public int amountOfPeaple { get; set; }
        public int daysToStay { get; set; }
        public OptionalCases optionalCases { get; set; }
        public User user { get; set; }
        public Accommodation accommodation { get; set; }
        public Reserve(int amountOfPeaple, User user, Accommodation accommodation, int daysToStay)
        {
            this.amountOfPeaple = amountOfPeaple;
            this.user = user;
            this.accommodation = accommodation;
            this.daysToStay = daysToStay;
            
        }

        public Reserve(int amountOfPeaple, User user, Accommodation accommodation, int daysToStay,OptionalCases optionalCases)
        {
            this.amountOfPeaple = amountOfPeaple;
            this.user = user;
            this.accommodation = accommodation;
            this.daysToStay = daysToStay;
            this.optionalCases = optionalCases;
        }
    }
}
