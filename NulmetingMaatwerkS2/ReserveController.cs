﻿using System;
using System.Collections.Generic;
using System.Text;
using NulmetingMaatwerkS2.HomeTypes;

namespace NulmetingMaatwerkS2
{
    public class ReserveController
    {
        public bool TryToAddReservation(Reserve reserve)
        {
            //looks if the input of the amountOfPeaple is not higher than the max amount of peaple
            if (reserve.amountOfPeaple > reserve.accommodation.amountOfPeaple)
            {
                return false;
            }
            else if (String.IsNullOrWhiteSpace(reserve.amountOfPeaple.ToString()) || reserve.accommodation == null || reserve.user.name == "" || reserve.daysToStay.ToString() == "")
            {
                //looks if the fields are not empty
                return false;
            }
            else if (reserve.accommodation.defect == null)
            {
                return true;
            }
            else if (reserve.accommodation.defect.defectType == "Groot" && reserve.accommodation.defect.repairman == null)//looks if the defection is to big to reserve the room
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public decimal CalculatePrice(Reserve reserve)
        {
            decimal price = 0;
            if (reserve.accommodation.homeType == HomeType.Bungalow)
            {
                if (reserve.accommodation.bungalow.unit == unitForPrice.perAcomadationPerNight)
                {   
                    price = reserve.daysToStay * reserve.accommodation.bungalow.price;
                    price += reserve.amountOfPeaple * (decimal)1.5;
                }

                if (reserve.accommodation.bungalow.unit == unitForPrice.PerPersonPerNight)
                {
                    price = (reserve.accommodation.bungalow.price * reserve.amountOfPeaple) * reserve.daysToStay;
                    price += reserve.amountOfPeaple * (decimal)1.5;

                }
            }

            if (reserve.optionalCases == OptionalCases.ChildBed)
            {
                price += reserve.daysToStay * 3;
            }
            return price;
        }

       
    }
}
