﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NulmetingMaatwerkS2
{
    public class Defect
    {
        public string nameDefect { get; set; }
        public DateTime anouncementDate { get; set; }
        public string defectType { get; set; }
        public string repairman { get; set; }

        public Defect(string nameDefect, DateTime anouncementDate, string defectType)
        {
            this.nameDefect = nameDefect;
            this.anouncementDate = anouncementDate;
            this.defectType = defectType;
        }

        public Defect(string nameDefect, DateTime anouncementDate, string defectType, string repairman)
        {
            this.nameDefect = nameDefect;
            this.anouncementDate = anouncementDate;
            this.defectType = defectType;
            this.repairman = repairman;
        }
    }
}
