﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NulmetingMaatwerkS2.HomeTypes
{
    public enum unitForPrice
    {
        perAcomadationPerNight,
        PerNight,
        PerPersonPerNight,
        PerSquareMeter
    }

    public class Bungalow
    {
        public int amountOfPeaple { get; set; }
        public decimal price { get; set; }
        public unitForPrice unit { get; set; }
        public int amountOfsquareMeters { get; set; }
        public string variant { get; set; }
        public bool nextToWater { get; set; }

        public Bungalow(int amountOfPeaple, decimal price, unitForPrice unit, int amountOfsquareMeters, string variant, bool nextToWater)
        {
            this.amountOfPeaple = amountOfPeaple;
            this.price = price;
            this.unit = unit;
            this.amountOfsquareMeters = amountOfsquareMeters;
            this.variant = variant;
            this.nextToWater = nextToWater;
        }

      


    }
}
