﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NulmetingMaatwerkS2.HomeTypes
{
    public class Tent
    {
        public int amountOfPeaple { get; set; }
        public decimal price { get; set; }
        public unitForPrice unit { get; set; }
        public int amountOfsquareMeters { get; set; }

        public Tent(int amountOfPeaple, decimal price, unitForPrice unit, int amountOfsquareMeters)
        {
            this.amountOfPeaple = amountOfPeaple;
            this.price = price;
            this.unit = unit;
            this.amountOfsquareMeters = amountOfsquareMeters;
        }
    }
}
