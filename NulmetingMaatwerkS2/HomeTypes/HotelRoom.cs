﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NulmetingMaatwerkS2.HomeTypes
{
    public class HotelRoom
    {
        public int amountOfPeaple { get; set; }
        public decimal price { get; set; }
        public unitForPrice unit { get; set; }
        public int amountOfsquareMeters { get; set; }
        public string facilitys { get; set; }
        public string bedType { get; set; }

        public HotelRoom(int amountOfPeaple, decimal price, unitForPrice unit, int amountOfsquareMeters, string facilitys, string bedType)
        {
            this.amountOfPeaple = amountOfPeaple;
            this.price = price;
            this.unit = unit;
            this.amountOfsquareMeters = amountOfsquareMeters;
            this.facilitys = facilitys;
            this.bedType = bedType;
        }
    }
}
