﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NulmetingMaatwerkS2;
using System;
using System.Collections.Generic;
using System.Text;
using NulmetingMaatwerkS2.HomeTypes;

namespace NulmetingMaatwerkS2.Tests
{
    [TestClass()]
    public class ReserveControllerTests
    {
        [TestMethod()]
        public void TryToAddReservationTestBungalow()
        {
            Bungalow bungalow = new Bungalow(2, 50, unitForPrice.perAcomadationPerNight, 60, "normal", true);
            Accommodation accommodation = new Accommodation(HomeType.Bungalow, bungalow.amountOfPeaple) { bungalow = bungalow };
            User user = new User("Bram");
            Reserve reserve = new Reserve(2,user,accommodation,5);

            ReserveController reserveController = new ReserveController();
            bool result = reserveController.TryToAddReservation(reserve);
            
            Assert.IsTrue(result);
        }

        
        [TestMethod()]
        public void TryToAddReservationTest_Fail()
        {
            Bungalow bungalow = new Bungalow(2, 50, unitForPrice.perAcomadationPerNight, 60, "normal", true);
            Accommodation accommodation = new Accommodation(HomeType.Bungalow, bungalow.amountOfPeaple) { bungalow = bungalow };
            User user = new User("Bram");
            Reserve reserve = new Reserve(5,user,accommodation,5);

            ReserveController reserveController = new ReserveController();
            bool result = reserveController.TryToAddReservation(reserve);

            Assert.IsFalse(result);
        }
        

        [TestMethod()]
        public void TryToAddReservationTestHotelRoom()
        {
            HotelRoom hotelRoom = new HotelRoom(2, 50, unitForPrice.perAcomadationPerNight, 25, "Douche", "1-2persoons");
            Defect defect = new Defect("Douche kapot", DateTime.Now, "Groot","Jan van den Bosch");
            Accommodation accommodation = new Accommodation(HomeType.HotelRoom, hotelRoom.amountOfPeaple) { hotelRoom = hotelRoom, defect = defect };
            User user = new User("Bram");
            Reserve reserve = new Reserve(2,user,accommodation,5);

            ReserveController reserveController = new ReserveController();
            bool result = reserveController.TryToAddReservation(reserve);

            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void TryToAddReservationTestHotelRoom_Fail()
        {
            HotelRoom hotelRoom = new HotelRoom(2, 50, unitForPrice.perAcomadationPerNight, 25, "Douche", "1-2persoons");
            Defect defect = new Defect("Douche kapot",DateTime.Now,"Groot");

            Accommodation accommodation = new Accommodation(HomeType.HotelRoom, hotelRoom.amountOfPeaple) { hotelRoom = hotelRoom,defect = defect};
            User user = new User("Bram");
            Reserve reserve = new Reserve(2,user,accommodation,5);

            ReserveController reserveController = new ReserveController();
            bool result = reserveController.TryToAddReservation(reserve);

            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void TryToAddReservationTestTent()
        {
            Tent tent = new Tent(5, 4, unitForPrice.PerSquareMeter, 14);
            Accommodation accommodation = new Accommodation(HomeType.Tent, tent.amountOfPeaple) { tent = tent};
            User user = new User("Bram");
            Reserve reserve = new Reserve(4,user,accommodation,5);

            ReserveController reserveController = new ReserveController();
            bool result = reserveController.TryToAddReservation(reserve);

            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void TryToAddReservationTestTent_fail()
        {
            Tent tent = new Tent(5, 4, unitForPrice.PerSquareMeter, 14);
            Accommodation accommodation = new Accommodation(HomeType.Tent, tent.amountOfPeaple) { tent = tent };
            User user = new User("");
            Reserve reserve = new Reserve(2,user,accommodation,5);

            ReserveController reserveController = new ReserveController();
            bool result = reserveController.TryToAddReservation(reserve);

            Assert.IsFalse(result);
        }


        [TestMethod()]
        public void CalculateReservation()
        {
            Bungalow bungalow = new Bungalow(5, 50, unitForPrice.perAcomadationPerNight, 14,"Normal",true);
            Accommodation accommodation = new Accommodation(HomeType.Bungalow, bungalow.amountOfPeaple) { bungalow = bungalow };
            User user = new User("Bram");
            Reserve reserve = new Reserve(2, user, accommodation, 5);

            ReserveController reserveController = new ReserveController();
            decimal result = reserveController.CalculatePrice(reserve);

            Assert.AreEqual(result,253);
        }

        [TestMethod()]
        public void CalculateReservation2()
        {
            Bungalow bungalow = new Bungalow(5, 50, unitForPrice.PerPersonPerNight, 14, "Normal", true);
            Accommodation accommodation = new Accommodation(HomeType.Bungalow, bungalow.amountOfPeaple) { bungalow = bungalow };
            User user = new User("Bram");
            Reserve reserve = new Reserve(2, user, accommodation, 5);

            ReserveController reserveController = new ReserveController();
            decimal result = reserveController.CalculatePrice(reserve);

            Assert.AreEqual(result, 503);
        }

        [TestMethod()]
        public void CalculateReservation3()
        {
            Bungalow bungalow = new Bungalow(2, 50, unitForPrice.PerPersonPerNight, 14, "Normal", true);
            Accommodation accommodation = new Accommodation(HomeType.Bungalow, bungalow.amountOfPeaple) { bungalow = bungalow };
            User user = new User("Bram");
            Reserve reserve = new Reserve(2, user, accommodation, 5,OptionalCases.ChildBed);

            ReserveController reserveController = new ReserveController();
            decimal result = reserveController.CalculatePrice(reserve);

            Assert.AreEqual(result, 518);
        }

        [TestMethod()]
        public void TryToAddNewAccommodation()
        {
            Bungalow bungalow = new Bungalow(2, 50, unitForPrice.PerPersonPerNight, 14, "Normal", true);

            AccomodationController accomodationController = new AccomodationController();
            bool result = accomodationController.AddNewBungalow(bungalow);

            Assert.IsTrue(result);
        }





    }
}